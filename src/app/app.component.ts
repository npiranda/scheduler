import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor() {
    const config = {
      apiKey: 'AIzaSyAP8Mw115iUrRKOIq0081jcPIU3XbEN3-g',
      authDomain: 'scheduler-a0d90.firebaseapp.com',
      databaseURL: 'https://scheduler-a0d90.firebaseio.com',
      projectId: 'scheduler-a0d90',
      storageBucket: 'scheduler-a0d90.appspot.com',
      messagingSenderId: '245224002826',
      appId: '1:245224002826:web:48978450df71817decd6f3',
      measurementId: 'G-WGYB43SEQ6'
    };
    firebase.initializeApp(config);
  }
}
